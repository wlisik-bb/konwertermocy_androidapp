package pl.lukaszignaciuk.konwertermocy;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private EditText wpisana_moc;
    private Spinner jednostka_spinner;
    private TextView[] wyniki_konwersji;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wpisana_moc = (EditText) findViewById(R.id.wpisana_moc);
        jednostka_spinner = (Spinner) findViewById(R.id.jednostka_spinner);
        wyniki_konwersji = new TextView[6];
        wyniki_konwersji[0] = (TextView) findViewById(R.id.gridlayout_textview_1);
        wyniki_konwersji[1] = (TextView) findViewById(R.id.gridlayout_textview_2);
        wyniki_konwersji[2] = (TextView) findViewById(R.id.gridlayout_textview_3);
        wyniki_konwersji[3] = (TextView) findViewById(R.id.gridlayout_textview_4);
        wyniki_konwersji[4] = (TextView) findViewById(R.id.gridlayout_textview_5);
        wyniki_konwersji[5] = (TextView) findViewById(R.id.gridlayout_textview_6);

        wypelnijSpinner();

        ustawListenerWpisanaMoc();

        ustawListenerSpinner();
    }

    private void ustawListenerWpisanaMoc() {
        wpisana_moc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!String.valueOf(s).equals(""))    przelicz(jednostka_spinner.getSelectedItemPosition());
            }
        });
    }

    private void ustawListenerSpinner() {
        jednostka_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                przelicz(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void przelicz(int position) {
        String wpisanaMocString = String.valueOf(wpisana_moc.getText()).replace(",", ".");
        try {
            switch (position) {
                case 0:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 1000.0)) + " kW");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 735.49875)) + " KM");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 745.69987)) + " BHP");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 0.293071)) + " BTU/h");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 17.584264)) + " BTU/m");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 1055.05585262)) + " BTU/s");
                    break;
                case 1:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 1000.0)) + " W");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 0.73549875)) + " KM");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 0.74569987)) + " BHP");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 0.000293071)) + " BTU/h");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 0.017584264)) + " BTU/m");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) / 1.05505585262)) + " BTU/s");
                    break;
                case 2:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 735.49875)) + " W");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.73549875)) + " kW");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.9863)) + " BHP");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 2509.6267)) + " BTU/h");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 41.8271)) + " BTU/m");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.6971)) + " BTU/s");
                    break;
                case 3:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 745.6999)) + " W");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.7457)) + " kW");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 1.0139)) + " KM");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 2544.4342)) + " BTU/h");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 42.4072)) + " BTU/m");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.7068)) + " BTU/s");
                    break;
                case 4:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.293071)) + " W");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.000293)) + " kW");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.000398)) + " KM");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.000393)) + " BHP");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.016647)) + " BTU/m");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.000277)) + " BTU/s");
                    break;
                case 5:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 17.5843)) + " W");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.0176)) + " kW");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.0239)) + " KM");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.0236)) + " BHP");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 60.0001)) + " BTU/h");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 0.0167)) + " BTU/s");
                    break;
                case 6:
                    wyniki_konwersji[0].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 1055.0559)) + " W");
                    wyniki_konwersji[1].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 1.0551)) + " kW");
                    wyniki_konwersji[2].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 1.434477)) + " KM");
                    wyniki_konwersji[3].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 1.414853)) + " BHP");
                    wyniki_konwersji[4].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 3600.000862)) + " BTU/h");
                    wyniki_konwersji[5].setText(String.valueOf(String.format("%.2f", Double.parseDouble(String.valueOf(wpisanaMocString)) * 60.0)) + " BTU/m");
                    break;
            }
        } catch (NumberFormatException ex){
            ex.printStackTrace();
        }
    }

    private void wypelnijSpinner() {
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.jednostki, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        jednostka_spinner.setAdapter(spinnerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
